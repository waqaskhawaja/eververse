# EverVerse #

EverVerse is a poetry generator based on physical activity. It connects to Activity Trackers
such as smart watches and generates verses based on the physical activity of the user.

### About This Repository ###

This repository hosts the source code for EverVerse generator. A working implementation
can be found at the [EverVerse website](http://www.eververse.com).

### Project Setup ###

These instructions are aimed at Debian based variants of the Linux systems. Please adjust according to your own
environment.

#### System Requirements ####

* You will need `$ Python 3` to run this project. Any version greater than `3.4` should be fine.
On Some systems, the command is available as `python3`. You can check version by typing `$ python3 --version`
* Install Python3 Virtual Environment `$ sudo apt-get install python3-venv`

#### Running the project ####

* Move to your home directory `$ cd ~`
* Create project directory `$ mkdir eververse`
* Clone source code `$ git clone https://waqaskhawaja@bitbucket.org/waqaskhawaja/eververse.git eververse`
* Change to project directory `$ cd eververse`
* Create python virtual environment `$ python -m venv venv-eververse`
* Install project requirements `$ venv-eververse/bin/pip install -r requirements.txt`
* Install NLTK data
    * `$ venv-eververse/bin/python`
    * `>>> import nltk`
    * `>>> nltk.download('punkt')`
    * `>>> exit()`
* Create Database `./db_create.py`
* Run the system `$ venv-eververse/bin/python ./run.py`
* You should be able to see the output by going to [http://localhost:5000](http://localhost:5000) in your browser.

#### Running an existing setup ####
* If you have an existing setup, please run `$ venv-eververse/bin/pip install requirements.txt` to ensure that you
have all the latest packages.

#### Admin Panel ####
* Settings for different heart zones can be configured in the [Admin Panel](http://localhost:5000/admin).

#### Building the Frontend ####

* `cd eververse/` and run `npm install` to install dependencies (depends on you having [https://npmjs.org](https://npmjs.org) installed)
* Run `gulp build` to build the frontend code, update the templates, etc

### View Options

Have added different "Modes" to view the project in:

1. Standard web view is at `/`
2. Exhibition mode is at `/exhibition`
3. Live Performance mode is at `/performance`, with a "waiting screen" at `/performance-waiting`

Both the exhibition & performance modes use pre-generated data  

### Production Deployment

* The project is hosted at https://eververse.nuigawlay.ie
* The project is served using Apache `mod_wsgi`
* For deploying a newer version
    * Create a tag on master branch `git tag -a v0.1.1 -m "message for release"`
    * Push tag to origin `git push origin tag v0.1.1`
    * Server login `ssh user@eververse.nuigalway.ie`
    * Download latest release `wget -c --user bitbucket_user --ask-password https://bitbucket.org/waqaskhawaja/eververse/get/v0.1.1.zip`
    * Unzip archive `unzip v0.1.1.zip -d v0.1.1`
    * Copy to deployment directory `sudo cp -R v0.1.1/* /opt/eververse/`
    * Restart Apache `sudo service apache2 restart`
