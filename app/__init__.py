import os
import os.path as op
from flask import Flask, Response, redirect
from flask_admin.form import FileUploadField
from flask_basicauth import BasicAuth
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from werkzeug.exceptions import HTTPException
from sqlalchemy.event import listens_for
from apscheduler.schedulers.background import BackgroundScheduler


RESOURCES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources')
CORPUS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static/corpus')
app = Flask(__name__)
app.config.from_object('config')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
basic_auth = BasicAuth(app)


from app import views, models
from app.models import HeartZone, Corpus, AuthInfo, SleepZone
from app.client.fitbit_client import sync_fitbit_server

# Create directory for file fields to use
file_path = op.join(op.dirname(__file__), 'static/corpus')
try:
    os.mkdir(file_path)
except OSError:
    pass


# Delete hooks for models, delete files if models are getting deleted
@listens_for(Corpus, 'after_delete')
def del_file(mapper, connection, target):
    if target.path:
        try:
            os.remove(op.join(file_path, target.path))
        except OSError:
            pass


class AuthException(HTTPException):
    def __init__(self, message):
        super().__init__(message, Response(
            "You could not be authenticated. Please refresh the page.", 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        ))


class ModelView(ModelView):
    def is_accessible(self):
        if not basic_auth.authenticate():
            raise AuthException('Not authenticated.')
        else:
            return True

    def inaccessible_callback(self, name, **kwargs):
        return redirect(basic_auth.challenge())


class CorpusView(ModelView):
    # Override form field to use Flask-Admin FileUploadField
    form_overrides = {
        'file_path': FileUploadField
    }

    # Pass additional parameters to 'path' to FileUploadField constructor
    form_args = {
        'file_path': {
            'label': 'Corpus',
            'base_path': file_path,
            'allow_overwrite': False
        }
    }


class HeartZoneView(ModelView):
    form_excluded_columns = {'sleep_zone'}


class SleepZoneView(ModelView):
    form_excluded_columns = {'sleep_entry'}


admin = Admin(app, name='EverVerse', template_mode='bootstrap3')
admin.add_view(HeartZoneView(HeartZone, db.session))
admin.add_view(SleepZoneView(SleepZone, db.session))
admin.add_view(CorpusView(Corpus, db.session))
admin.add_view(ModelView(AuthInfo, db.session))


if __name__ == "__main__":
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=sync_fitbit_server, trigger="interval", hours=24)
    scheduler.start()
    app.run()
