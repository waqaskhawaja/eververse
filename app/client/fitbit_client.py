import json
import requests
from pathlib import Path
from datetime import date, timedelta, datetime as dt
from sqlalchemy import func
from app import RESOURCES_DIR
from app.models import AuthInfo, db, HeartEntry, SleepEntry, SleepZone


refresh_url = 'https://api.fitbit.com/oauth2/token'
scope = ['sleep', 'heart']
url_end = '.json'
date_sleep_data_url = 'https://api.fitbit.com/1.2/user/-/sleep/date/'
date_range_sleep_data_url = 'https://api.fitbit.com/1.2/user/-/sleep/date/[startDate]/[endDate].json'
date_heart_rate_url = 'https://api.fitbit.com/1/user/-/activities/heart/date/'
date_heart_rate_url_post_text = '/1d'


def token_updater(token):
    auth_info = AuthInfo.query.first()
    auth_info.access_token = token['access_token']
    auth_info.refresh_token = token['refresh_token']
    db.session.commit()


def get_heart_data(date_to_get):
    header = {
        'Authorization': 'Bearer ' + AuthInfo.query.first().access_token,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    date_to_get = date_to_get.strftime("%Y-%m-%d")
    request_url = date_heart_rate_url + date_to_get + date_heart_rate_url_post_text + url_end
    r = requests.get(request_url, headers=header)
    if r.status_code == 200:
        return json.loads(r.content)
    else:
        renew_token()
        return False


def get_sleep_data(date_to_get):
    header = {
        'Authorization': 'Bearer ' + AuthInfo.query.first().access_token,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    date_to_get = date_to_get.strftime("%Y-%m-%d")
    request_url = date_sleep_data_url + date_to_get + url_end
    r = requests.get(request_url, headers=header)
    if r.status_code == 200:
        return json.loads(r.content)
    else:
        renew_token()
        return False


def read_fitbit():
    files = Path(RESOURCES_DIR+"/fitbit-data/2017/")
    directory_names = []
    for single_file in files.iterdir():
        if single_file.is_dir():
            directory_names.append(str(single_file))
            directory_names.sort(reverse=True)
    current_file = open(directory_names[0]+'/heart.json')
    file_data_as_list = current_file.readlines()
    file_data_as_string = ''.join(file_data_as_list)
    json_hearts = json.loads(file_data_as_string)
    items = json_hearts["activities-heart-intraday"]["dataset"]
    return items


# def read_sleep():
#     day = datetime.today() - timedelta(days=1)
#     print(authd_client.get_sleep(day))


def renew_token():
    header = {
        'Authorization': 'Basic MjJEQ0ZZOjUwMjZiZmE1NjEwNWU0MWMwZTg2MzYzYzAyOTdmNzdi',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    body = {
        'client_id': '22DCFY',
        'grant_type': 'refresh_token',
        'refresh_token': AuthInfo.query.first().refresh_token
    }
    r = requests.post(refresh_url, data=body, headers=header)
    print(r.content)
    token_updater(json.loads(r.content))


def get_total_heart_zone_dates():
    heart_zone_dates_in_db = []
    heart_zone_results = db.session.query(func.distinct(HeartEntry.entry_date_time)).all()
    for i in heart_zone_results:
        heart_zone_dates_in_db.append(i[0].split(' ')[0])
    return heart_zone_dates_in_db


# dates_in_db.append(datetime.strptime(str(i[0]), '%Y-%m-%d %H:%M:%S.%f'))
# dates_to_sync.append(datetime.strptime(d1 + timedelta(i), '%Y-%m-%d'))
def sync_fitbit_server():
    print('string sync...')
    total_dates = []
    heart_zone_dates_in_db = get_total_heart_zone_dates()
    sleep_zone_dates_in_db = []
    print('found ' + str(len(heart_zone_dates_in_db)) + ' heart zone unique days record in db')
    today = date.today()
    d1 = date(2019, 2, 1)  # start date
    d2 = date(today.year, today.month, today.day)  # end date
    delta = d2 - d1
    for i in range(delta.days + 1):
        total_dates.append(dt.strftime(d1 + timedelta(i), '%Y-%m-%d'))
    dates_to_sync = set(total_dates) - set(heart_zone_dates_in_db)
    print('going to sync heart zone data for ' + str(len(dates_to_sync)) + ' days.')
    for single_date in dates_to_sync:
        single_date = dt.strptime(single_date, '%Y-%m-%d')
        print('staring ' + str(single_date))
        json_content = get_heart_data(single_date)
        if not json_content:
            #token was expired which should have been renewed, so calling method again
            json_content = get_heart_data(single_date)
        print('going to save ' + str(len(json_content["activities-heart-intraday"]["dataset"])) + ' records in database')
        for heart_entry_json in json_content["activities-heart-intraday"]["dataset"]:
            heart_entry_db = HeartEntry()
            heart_entry_db.entry_date_time = dt.combine(single_date, dt.strptime(heart_entry_json["time"], '%H:%M:%S').time())
            heart_entry_db.value = heart_entry_json["value"]
            db.session.add(heart_entry_db)
        db.session.commit()
        single_date_records = HeartEntry.query.filter(func.DATE(HeartEntry.entry_date_time) == single_date.date()).all()
        print('saved ' + str(len(single_date_records)) + ' records')

    sleep_zone_results = db.session.query(func.distinct(SleepEntry.entry_date_time)).all()
    for i in sleep_zone_results:
        sleep_zone_dates_in_db.append(i[0].split(' ')[0])
    print('found ' + str(len(sleep_zone_dates_in_db)) + ' sleep zone unique days record in db')
    dates_to_sync = set(total_dates) - set(sleep_zone_dates_in_db)
    print('going to sync sleep zone data for ' + str(len(dates_to_sync)) + ' days.')
    for single_date in dates_to_sync:
        single_date = dt.strptime(single_date, '%Y-%m-%d')
        print('staring ' + str(single_date))
        json_content = get_sleep_data(single_date)
        print('going to save ' + str(len(json_content["sleep"][0]["levels"]["data"])) + ' records in database')
        for sleep_entry_json in json_content["sleep"][0]["levels"]["data"]:
            sleep_entry_db = SleepEntry()
            sleep_entry_db.entry_date_time = dt.strptime(sleep_entry_json["dateTime"], '%Y-%m-%dT%H:%M:%S.%f')
            sleep_entry_db.sleep_zone = SleepZone.query.filter(SleepZone.sleep_zone_name == sleep_entry_json["level"]).first()
            db.session.add(sleep_entry_db)
        db.session.commit()
        single_date_records = SleepEntry.query.filter(HeartEntry.entry_date_time == single_date).all()
        print('saved ' + str(len(single_date_records)) + ' records')

