from nltk import WhitespaceTokenizer
from app import RESOURCES_DIR
from app import CORPUS_DIR
from app.poets.poesy.Poesy import reverseNgrams, setupModel


def generate_model(sentences):
    stop_words_list = open(RESOURCES_DIR+"/stop_words.txt", encoding="utf-8").readlines()
    ngrams = []
    stop_words = set(stop_words_list)
    for sentence in sentences:
        filtered_sentence = []
        words = WhitespaceTokenizer().tokenize(sentence)
        for word in words:
            if word not in stop_words:
                filtered_sentence.append(word)
        ngrams += reverseNgrams(filtered_sentence, 3)
    model = setupModel(ngrams)
    return model


def get_zone_model(corpus):
    corpus_file = open(CORPUS_DIR+"/"+corpus, encoding="utf-8")
    return generate_model(corpus_file.readlines())


single_file = open(RESOURCES_DIR+"/combined_corpus.txt", encoding="utf-8")
model = generate_model(single_file.readlines())
