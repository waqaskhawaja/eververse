from app.poets.poesy.Poesy import generateCouplet, poemProcessing
from app import generator


def write_poetry(lines, words, corpus):
    model = generator.get_zone_model(corpus)
    poem = generateCouplet(model, lines, words)
    poemProcessing(poem)
    return poem

