from sqlalchemy.orm import relationship

from app import db


class HeartZone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    zone_name = db.Column(db.String(256), index=True, unique=True)
    number_of_words = db.Column(db.Integer)
    min_val = db.Column(db.Integer)
    max_val = db.Column(db.Integer)
    frequency_in_seconds = db.Column(db.Integer)
    corpus_id = db.Column(db.Integer, db.ForeignKey('corpus.id'))
    corpus = relationship("Corpus", back_populates="heart_zone", uselist=False)
    sleep_zone = relationship("SleepZone", back_populates="heart_zone")

    def __repr__(self):
        return self.zone_name


class SleepZone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sleep_zone_name = db.Column(db.String(256), index=True, unique=True)
    number_of_words = db.Column(db.Integer)
    frequency_in_seconds = db.Column(db.Integer)
    heart_zone_id = db.Column(db.Integer, db.ForeignKey('heart_zone.id'))
    heart_zone = relationship("HeartZone", back_populates="sleep_zone", uselist=False)
    sleep_entry = relationship("SleepEntry", back_populates="sleep_zone")
    corpus_id = db.Column(db.Integer, db.ForeignKey('corpus.id'))
    corpus = relationship("Corpus", back_populates="sleep_zone", uselist=False)

    def __unicode__(self):
        return self.sleep_zone_name

    def __repr__(self):
        return self.sleep_zone_name


# Create models
class Corpus(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    corpus_name = db.Column(db.Unicode(64))
    file_path = db.Column(db.Unicode(128))
    heart_zone = relationship("HeartZone", back_populates="corpus")
    sleep_zone = relationship("SleepZone", back_populates="corpus")

    def __unicode__(self):
        return self.corpus_name

    def __repr__(self):
        return self.corpus_name


class AuthInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Unicode(32), unique=True)
    client_secret = db.Column(db.Unicode(64))
    access_token = db.Column(db.Unicode(500))
    refresh_token = db.Column(db.Unicode(128))

    def __repr__(self):
        return self.client_id


class SleepEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    entry_date_time = db.Column(db.DateTime)
    sleep_zone_id = db.Column(db.Integer, db.ForeignKey('sleep_zone.id'))
    sleep_zone = relationship("SleepZone", back_populates="sleep_entry", uselist=False)

    def __repr__(self):
        return str(self.entry_date_time)


class HeartEntry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    entry_date_time = db.Column(db.DateTime)
    value = db.Column(db.Integer)

    def __repr__(self):
        return str(self.entry_date_time)

