When first described imperfectly
by my shy mother, I tried to leap

from the moving
car. A response,

I suspect, of not
just terror (although

a kind of terror continues to play
its part), but also a mimetic gesture,

the expression equal
to a body's system of absurd

jokes and dirty stories.
With cockeyed breasts

peculiar as distant cousins,
and already the butt of the body's

frat-boy humor,
I'd begun to pack

a bag, would set off
soon for my separate

country. Now, sometimes,
I admire the surprised engineering:

how a man's body can rise,
squaring off with the weight

of gravity, single-minded,
exposed as the blind

in traffic. It's the body leaping
that I praise, vulnerable

in empty space.
It's mapping the empty

space; a man's life driving
down a foreign road.