Maybe I'm seven in the open field--
the straw-grass so high
only the top of my head makes a curve
of brown in the yellow. Rain then.
First a little. A few drops on my
wrist, the right wrist. More rain.
My shoulders, my chin. Until I'm looking up
to let my eyes take the bliss.
I open my face. Let the teeth show. I
pull my shirt down past the collar-bones.
I'm still a boy under my breast spots.
I can drink anywhere. The rain. My
skin shattering. Up suddenly, needing
to gulp, turning with my tongue, my arms out
running, running in the hard, cold plentitude
of all those who reach earth by falling.