 Skin is the closest thing to god,
touching oil, clay,
intimate with the foreign land of air
and other bodies,
places not in light,
lonely
for its own image.

It is awash in its own light.
It wants to swim and surface
from the red curve of the sea,
open all its eyes.

Skin is the oldest thing.
It remembers when it was the cold
builder of fire,
when darkness was the circle around it,
when there were eyes shining in the night,
a breaking twig, and it rises
in fear, a primitive lord on new bones.

I tell you, it is old,
it heals and is sometimes merciful.
It is water.
It has fallen through ancestral hands.
It is the bearer of vanished forest
fallen through teeth and jaws
of earth
where we were once other
visions and creations.