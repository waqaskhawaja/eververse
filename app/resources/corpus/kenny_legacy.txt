    my face is grass
    color of April rain;
    arms, legs are the limbs
    of birch, cedar;
    my thoughts are winds
    which blow;
    pictures in my mind
    are the climb up hill
    to dream in the sun;
    hawk feathers, and quills
    of porcupine running
    the edge of the stream
    which reflects stories
    of my many mornings
    and the dark faces of night
    mingled with victories
    of dawn and tomorrow;
    corn of the fields and squash…
    the daughters of my mother
    who collect honey
    and all the fruits;
    meadow and sky are the end of my day,
    the stretch of my night
    yet the birth of my dust;
    my wind is the breath of a fawn
    the cry of the cub
    the trot of the wolf
    whose print covers
    the tracks of my feet;
    my word, my word,
    loaned
    legacy, the obligation I hand
    to the blood of my flesh
    the sinew of the loins
    to hold to the sun
    and the moon
    which direct the river
    that carries my song
    and the beat of the drum
    to the fires of the village
    which endures.