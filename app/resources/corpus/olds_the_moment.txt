When I saw the dark Egyptian stain,
I went down into the house to find you, mother
Past the grandfather clock, with its huge ochre moon
Past the burnt sienna woodwork, rubbed and glazed
I went deeper and deeper down into the body of the house
Down below the level of the earth
And I found you there where I had never found you
By the washtubs, your hand thrust deep in soapy water
And above your head the blazing windows at the circus of the ground
You looked up from the iron sink,
A small haggard pretty woman of 40, one week divorced
"I’ve got my period mom," I said and saw your face
abruptly break open and glow with joy
"Baby," you said, coming toward me, hands out
And covered with tiny delicate bubbles like seeds