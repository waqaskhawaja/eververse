In the darkened moment a body gifted with the blue light of a flashlight
enters with levity, with or without assumptions, doubts, with desire,
the beating heart, disappointment, with desires—

Stand where you are.

You begin to move around in search of the steps it will take before you
are thrown back into your own body, back into your own need to be found.

Destinations are lost. You raise yourself. No one else is seeking.

You exhaust yourself looking into the blue light. All day blue burrows
the atmosphere. What doesn’t belong with you won’t be seen.

You could build a world out of need or you could hold everything
back and see. You could hold everything back. You hold back the black.

You hold everything black. You hold this body’s lack. You hold yourself
back until nothing’s left but the dissolving blues of metaphor.