Indelicate is he who loathes
The aspect of his fleshy clothes, --
The flying fabric stitched on bone,
The vesture of the skeleton,
The garment neither fur nor hair,
The cloak of evil and despair,
The veil long violated by
Caresses of the hand and eye.
Yet such is my unseemliness:
I hate my epidermal dress,
The savage blood's obscenity,
The rags of my anatomy,
And willingly would I dispense
With false accouterments of sense,
To sleep immodestly, a most
Incarnadine and carnal ghost.