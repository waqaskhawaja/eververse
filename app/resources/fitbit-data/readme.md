#EverVerse FitBit data

Updated: 14/7/17 - DK

A sample of daily data dumps extracted from the [FitBit API](dev.fitbit.com/docs) for the user: **cnstntpt@gmail.com**. Data output is from the "[intraday time series](https://dev.fitbit.com/docs/activity/#get-activity-intraday-time-series)" API, which returns data at a 1-minute detail level. 

##Data samples

In the `2017` directory, you'll find daily dumps covering the period 18th May - 13th July. Each day contains a set of JSON files for the following:

- activities/calories
- activities/distance
- activities/floors
- activities/heart
- activities/sleep
- activities/steps

###Samples of data:

####activities/calories
- data file: `calories.json` 
- resource path: `activities/calories` 
- Docs: [https://dev.fitbit.com/docs/activity/](https://dev.fitbit.com/docs/activity/)
- Sample data:

```
{
    "activities-calories": [
        {
          "value": "2402", 
          "dateTime": "2017-05-18"
        }
    ], 
    "activities-calories-intraday": {
        "datasetType": "minute", 
        "datasetInterval": 1, 
        "dataset": [
            {
                "mets": 10, 
                "time": "00:00:00", 
                "value": 1.100600004196167, 
                "level": 0
            },
            {...}
        ]
    }
}
```

####activities/distance
- data file: `distance.json` 
- resource path: `activities/distance` 
- Docs: [https://dev.fitbit.com/docs/activity/](https://dev.fitbit.com/docs/activity/)
- Sample data:

```
{
    "activities-distance-intraday": {
        "datasetType": "minute", 
        "datasetInterval": 1, 
        "dataset": [
            {...}
          {
            "value": 0.004536009859293699, 
            "time": "00:38:00"
          },
          {...}
        ]
    }, 
    "activities-distance": [
        {
          "value": "3.9531945920636", 
          "dateTime": "2017-05-18"
        }
  ]
}
```

####activities/floors
- data file: `floors.json` 
- resource path: `activities/floors` 
- Docs: [https://dev.fitbit.com/docs/activity/](https://dev.fitbit.com/docs/activity/)
- Sample data:

```
{
    "activities-floors": [
        {
          "value": "26", 
          "dateTime": "2017-05-18"
        }
    ], 
    "activities-floors-intraday": {
        "datasetType": "minute", 
        "datasetInterval": 1, 
        "dataset": [      
          {
            "value": 1, 
            "time": "22:02:00"
          }, 
          {
            "value": 0, 
            "time": "22:03:00"
          }
        ]
    }
}
```

####activities/heart
- data file: `heart.json` 
- resource path: `activities/heart` 
- Docs: [https://dev.fitbit.com/docs/heart-rate/#get-heart-rate-intraday-time-series/](https://dev.fitbit.com/docs/heart-rate/#get-heart-rate-intraday-time-series/)
- Sample data:

```json
{
  "activities-heart": [
    {
      "value": {
        "restingHeartRate": 55, 
        "heartRateZones": [
          {
            "max": 91, 
            "caloriesOut": 2036.44018, 
            "minutes": 1285, 
            "name": "Out of Range", 
            "min": 30
          }, 
          {
            "max": 128, 
            "caloriesOut": 236.29882, 
            "minutes": 38, 
            "name": "Fat Burn", 
            "min": 91
          }, 
          {
            "max": 155, 
            "caloriesOut": 0, 
            "minutes": 0, 
            "name": "Cardio", 
            "min": 128
          }, 
          {
            "max": 220, 
            "caloriesOut": 0, 
            "minutes": 0, 
            "name": "Peak", 
            "min": 155
          }
        ], 
        "customHeartRateZones": []
      }, 
      "dateTime": "2017-05-18"
    }
  ], 
  "activities-heart-intraday": {
    "datasetType": "minute", 
    "datasetInterval": 1, 
    "dataset": [
      {
        "value": 56, 
        "time": "00:00:00"
      }, 
      {
        "value": 56, 
        "time": "00:01:00"
      },
      {...}      
    ]
  }
}
```

####activities/sleep
- data file: `sleep.json` 
- resource path: `activities/sleep` 
- Docs: [https://dev.fitbit.com/docs/sleep/](https://dev.fitbit.com/docs/sleep/)
- Sample data:

```json
{
  "sleep": [
    {
      "logId": 14684243339, 
      "dateOfSleep": "2017-05-18", 
      "minutesToFallAsleep": 0, 
      "awakeningsCount": 8, 
      "minutesAwake": 11, 
      "timeInBed": 497, 
      "minutesAsleep": 485, 
      "awakeDuration": 3, 
      "efficiency": 98, 
      "isMainSleep": true, 
      "startTime": "2017-05-17T22:31:00.000", 
      "restlessCount": 7, 
      "duration": 29820000, 
      "restlessDuration": 9, 
      "minuteData": [
        {
          "value": "1", 
          "dateTime": "22:31:00"
        }, 
        {
          "value": "1", 
          "dateTime": "22:32:00"
        }, 
        {...}
      ], 
      "endTime": "2017-05-18T06:48:00.000", 
      "awakeCount": 1, 
      "minutesAfterWakeup": 1
    }
  ], 
  "summary": {
    "totalTimeInBed": 497, 
    "stages": {
      "light": 249, 
      "wake": 48, 
      "deep": 73, 
      "rem": 127
    }, 
    "totalMinutesAsleep": 485, 
    "totalSleepRecords": 1
  }
}
```

####activities/steps
- data file: `steps.json` 
- resource path: `activities/steps` 
- Docs: [https://dev.fitbit.com/docs/activity/](https://dev.fitbit.com/docs/activity/)
- Sample data:

```
{
  "activities-steps-intraday": {
    "datasetType": "minute", 
    "datasetInterval": 1, 
    "dataset": [
      {...},
      {
        "value": 5, 
        "time": "22:10:00"
      }, 
      {
        "value": 6, 
        "time": "22:11:00"
      }, 
      {
        "value": 4, 
        "time": "22:12:00"
      },
      {...}      
    ]
  }, 
  "activities-steps": [
    {
      "value": "8609", 
      "dateTime": "2017-05-18"
    }
  ]
}
```
