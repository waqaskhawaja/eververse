//
//  https://stackoverflow.com/questions/29793849/how-to-create-a-simple-l-system-in-processing
//
function PenroseLSystem(){

  this.steps = 0;
  this.somestep = 0.1; 
  this.ruleW = 'YF++ZF4-XF[-YF4-WF]++';
  this.ruleX = '+YF--ZF[3-WF--XF]+';
  this.ruleY = '-WF++XF[+++YF++ZF]-';
  this.ruleZ = '--YF++++WF[+ZF++++XF]--XF';
  
  this.axiom = '[X]++[X]++[X]++[X]++[X]';
  this.rule = 'F+F-F';
  this.production = '';

  this.startLength = 460;
  // this.startLength = width;
  // this.theta = radians(38);  
  this.theta = radians(24);  
  
  this.generations = 0; 

  
  this.reset = function() {
    this.production = this.axiom;
    this.drawLength = this.startLength;
    this.generations = 0;
  }

  this.reset();

  this.getAge = function() {
    return this.generations;
  }

  this.iterate = function(prod_,rule_) {
    var newProduction = '';
    for (var i = 0; i < prod_.length; i++) {
      var step = this.production.charAt(i);
      if (step == 'W') {
        newProduction = newProduction + this.ruleW;
      } 
      else if (step == 'X') {
        newProduction = newProduction + this.ruleX;
      }
      else if (step == 'Y') {
        newProduction = newProduction + this.ruleY;
      }  
      else if (step == 'Z') {
        newProduction = newProduction + this.ruleZ;
      } 
      else {
        if (step != 'F') {
          newProduction = newProduction + step;
        }
      }
    }

    this.drawLength = this.drawLength * 0.5;
    this.generations++;
    return newProduction;
  }
  
  this.simulate = function(gen) {
    while (this.getAge() < gen) {
      this.production = this.iterate(this.production, this.rule);
    }
  }

  this.render = function(strokeColor, x, y) {
    // translate(width/2, height/2);
    translate(x, y);
    var pushes = 0;
    var repeats = 1;
    this.steps += 12;          
    if (this.steps > this.production.length) {
      this.steps = this.production.length;
    }
    // console.log(this.steps);

    for (var i = 0; i < this.steps; i++) {
      var step = this.production.charAt(i);//TODO you were here
      // console.log(step);
      var stepCode = this.production.charCodeAt(i);
      if (step == 'F') {
        stroke(strokeColor, 60);
        for (var j = 0; j < repeats; j++) {
          line(0, 0, 0, -this.drawLength);
          noFill();
          translate(0, -this.drawLength);
        }
        repeats = 1;
      } 
      else if (step == '+') {
        for (var j = 0; j < repeats; j++) {
          rotate(this.theta);
        }
        repeats = 1;
      } 
      else if (step == '-') {
        for (var j =0; j < repeats; j++) {
          rotate(-this.theta);
        }
        repeats = 1;
      } 
      else if (step == '[') {
        pushes++;            
        //pushMatrix();
        push();
      } 
      else if (step == ']') {
        //popMatrix();
        pop();
        pushes--;
      } 
      else if ( (stepCode >= 48) && (stepCode <= 57) ) {
        repeats = stepCode - 48;
      }
    }

    // Unpush if we need too
    while (pushes > 0) {
      // popMatrix();
      pop();
      pushes--;
    }
  }


};