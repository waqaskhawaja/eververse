/**
 * Code related to production of the generative 
 * design elements of Eververse
 *
 * @author David Kelly
 * 15/1/2018
 */

var eververseConfig = {    
    
    log_poem_data: false,           // do we want to save the text in backend?

    isPerformance: false,           // is this for a public performance?
    
    loop_text: false,               // run as a loop? (once end of data file is reached, go back to start)

    currentHeartRateValue: 80,        // outOfRange
    // currentHeartRateValue: 110,       // Fat Burn  
    // currentHeartRateValue: 145,          // Cardio
    // currentHeartRateValue: 156,       // Peak /* TODO */
    restingHeartRate: 56,
    heartRateZones: [
        {
            'max': 91, 
            'name': 'OutOfRange', 
            'min': 30
        }, 
        {
            'max': 128, 
            'name': 'Fat Burn', 
            'min': 91
        }, 
        {
            'max': 155, 
            'name': 'Cardio', 
            'min': 128
        }, 
        {
            'max': 220, 
            'name': 'Peak', 
            'min': 155
        }
    ],

    updateHeartRate: function(newHeartRate)
    {
        this.currentHeartRateValue = newHeartRate;
        setup();
    },


    // config for outOfRange Vis
    degs: 360,
    nRadius: 2,    
    outRadius: 71,
    totalTweens: 17.0,
    inBetween: 30.0,
    circleNumber: 10,
    background: 255,
    models: 300,
    radii: [],
    masterX: -300,
    masterY: 200
};

// Used in Peak vis
var ds;

// cardio maze
var x = 0,
    y = 0,
    position = 0,
    visOffset = 50,
    poetryWidth = 0,
    gap = 0,
    deg = 0;


/**
 * Object used to manage the visualisation-related
 * functionality of Eververse
 */
var eververseVis = {

    /**
     * get the name of the current heart-rate
     * zone. Returns default if none known.
     * 
     * @return string | default 'OutOfRange'
     */
    getCurrentZoneName: function(){
        var current = eververseConfig.currentHeartRateValue;

        var zone = _.find(eververseConfig.heartRateZones, function(value){
        
            return (current >= +value.min) && (current <= value.max);
        
        });

        if(zone){
            return zone.name;
        }
        return 'OutOfRange';
    },

    /**
     * get the name of the current heart-rate
     * zone. Returns default if none known.
     * 
     * @return string | default 'OutOfRange'
     */
    getCurrentZone: function(){
        var current = eververseConfig.currentHeartRateValue;

        var zone = _.find(eververseConfig.heartRateZones, function(value){
        
            return (current >= +value.min) && (current <= value.max);
        
        });

        if(zone){
            return zone;
        }
        return eververseConfig.heartRateZones[0];
    },

    // 
    // =============================================
    // 
    
    // ----------------------------------
    //      Setup for Out Of Range band
    // ----------------------------------
    visOutOfRangeConfig: function()
    {
        angleMode(RADIANS);
        randomSeed(eververseConfig.currentHeartRateValue);

        eververseConfig.nRadius = floor(random(1.0, 5.0));
        eververseConfig.outRadius = floor(random(100, 700));

        eververseConfig.totalTweens = floor(random(5, 100));
        eververseConfig.inBetween = floor(random(5, 100));
        eververseConfig.circleNumber = floor(random(1, 30));
        eververseConfig.models = floor(random(100, 500));

        doSetupModels();
//      eververseConfig.masterX=400;
        eververseConfig.masterX=0;
        eververseConfig.masterY = 150;
        
        
        strokeWeight(0.2);
        
        fill(255, 4);
    },

    // ----------------------------------
    //      Draw Out Of Range band
    // ----------------------------------
    visOutOfRange: function()
    {       

        if (eververseConfig.inBetween < eververseConfig.totalTweens && eververseConfig.circleNumber+1 < eververseConfig.models) {
            var hybridCircle = [];
            hybridCircle=doInterpolate(eververseConfig.radii[eververseConfig.circleNumber], eververseConfig.radii[eververseConfig.circleNumber+1], eververseConfig.inBetween/eververseConfig.totalTweens);
            doSimpleOutline(hybridCircle);
            eververseConfig.inBetween++;

        }else {
            if (eververseConfig.circleNumber+1 < eververseConfig.models) { 
                eververseConfig.circleNumber++;
                eververseConfig.inBetween=0;
            }
        }
    },


    // =============================================

    // ----------------------------------
    //      Setup for "FatBurn" band
    // ----------------------------------
    visFatBurnConfig: function()
    {
        translate(width * .33, height * .25);
        // noLoop();
        stroke(255);
        angleMode(DEGREES);
    },

    // ----------------------------------
    //      Draw "FatBurn" band
    // ----------------------------------
    visFatBurn: function()
    {

        // rotate(45/2);
        this.branchComponent(120, 12, 60);
    },

    // =============================================

    // ----------------------------------
    //      Setup for "Cardio" band
    // ----------------------------------
    visCardioConfig: function()
    {
        background(255);
        
        position = $('#poetry').offset(),
        visOffset = 50;
        y = position.top - visOffset ;
        x = position.left - visOffset ;
        poetryWidth = (x + $('#poetry').width() ) + (visOffset * 2);
        gap = 20;
        deg = 0;

        $('#poetry').css({'background': 'rgba(255,255,255,0.95)'});
        
    },

    // ----------------------------------
    //      Draw "Cardio" band
    // ----------------------------------
    visCardio: function()
    {
        var zone = eververseVis.getCurrentZone();
        var colCalc = Math.floor(eververseConfig.currentHeartRateValue * random( zone.min - 100, zone.min));
        // console.log(colCalc);

        var strokeColor = eververseVis.getColor(colCalc);
        // var strokeColor = eververseVis.getColor(eververseConfig.currentHeartRateValue);

        stroke(strokeColor);
        strokeWeight(2);
        
        // gap = eververseConfig.currentHeartRateValue / random(1, 50);
        gap = map(eververseConfig.currentHeartRateValue, zone.min, zone.max, random(10,30), random(40,80));

        if (random(2) < 0.5) { // The if statement changes the direction of the lines.
            line(x, y, x + gap, y + gap);
        } else {
            line(x, y + gap, x + gap, y);
        }
        
        x = x+ 10;                  //This allows the lines to go across the canvas.

        if (x > poetryWidth) {
            x = position.left - 50;
            y = y + gap;
        }
    },

    // 
    // =============================================
    // 

    // ----------------------------------
    //      Setup for "Peak" band
    // ----------------------------------
    visPeakConfig: function()
    {
        // frameRate(5);
        ds = new PenroseLSystem();
        ds.simulate(4);
        background(255);
    },

    // ----------------------------------
    //      Draw "Peak" band
    // ----------------------------------
    visPeak: function()
    {
        
        var zone = eververseVis.getCurrentZone();
        
        var colCalc = Math.floor(eververseConfig.currentHeartRateValue * random( zone.min - 100, zone.min));
        var strokeColor = eververseVis.getColor(colCalc);
        // console.log(colCalc, strokeColor);
        
        // Positioning of the vis...
        eververseConfig.masterX = width * 0.4;
        eververseConfig.masterY = height * 0.33;

        ds.render(strokeColor, eververseConfig.masterX, eververseConfig.masterY);
    },

    // 
    // =============================================
    // 
    

    getColor: function(xPos)
    {
        var currentHeartRateValue = eververseConfig.currentHeartRateValue;
        
        var currentZoneName = eververseVis.getCurrentZoneName();
        var zone = eververseVis.getCurrentZone();

        var col,
            rColVal,
            gColVal,
            bColVal;
        
        switch(currentZoneName){
            case 'Fat Burn':
                // rColVal = round(map(xPos, 0, width, 0, 60));
                // col=color(rColVal, 150, 100);
                
                rColVal = round(map(xPos, zone.min, random(zone.min, zone.max), 183, 255));
                gColVal = round(map(xPos, zone.min, random(zone.min, zone.max), 0, 150));
                bColVal = round(map(xPos, zone.min, random(zone.min, zone.max), 5, 80));

                col=color(rColVal, gColVal, bColVal);

                break;
            case 'Cardio':
                rColVal = round(map(xPos, 6000, 20000, 183, 255));
                gColVal = round(map(xPos, 6000, 20000, 0, 150));
                bColVal = round(map(xPos, 6000, 20000, 5, 80));

                col=color(rColVal, gColVal, bColVal);
                break;
            case 'Peak':
                // rColVal = round(map(xPos, 0, width, 0, 60));
                rColVal = round(map(xPos, 2000, 20000, 190, 255));
                gColVal = round(map(xPos, 2000, 20000, 27, 55));
                bColVal = round(map(xPos, 2000, 20000, 75, 120));

                // console.log(xPos, rColVal, gColVal, bColVal);
                // col=color(rColVal, 50, 200);
                // col=color(rColVal, 50, bColVal);
                col=color(rColVal, gColVal, bColVal);
                break;

            case 'OutOfRange':
            default: 
                rColVal = round(map(xPos, 0, width, 80, 255));
                col=color(rColVal, 255, 180);
                break;
        }
        
        return col;
    },


    branch: function(len, angle, gen, zone) {
        // console.log(len, angle, gen);
        
        var colorValPosition = eververseConfig.currentHeartRateValue;
        var strokeColor = this.getColor(Math.floor(colorValPosition));
        // console.log(strokeColor);
        stroke(strokeColor);

        line(0, 0, 0, -len);
        translate(0, -len);
        len *= 0.7;
        angle = random(angle-30, angle+20);

        if (len > 2) {
            push();
            rotate(angle);
            this.branch(len, angle, gen, zone);
            pop();

            push(); 
            rotate(-angle);
            this.branch(len, angle, gen, zone);
            pop();
        }
    },

    branchComponent: function(len, amount, angle) {

        
        var zone = eververseVis.getCurrentZone();
        var increment = 360/amount;
        var rotAmount;

        for (var i = 0; i < amount; i++) {
            push();
            rotAmount = -180 + increment * i
            rotate(random(rotAmount - 60, rotAmount));
            this.branch(len, angle, 1, zone);
            pop();
        }
    }


};



function setup()
{
    // colorMode(RGB);
    createCanvas (windowWidth, windowHeight);
    smooth();
    
    var band = eververseVis.getCurrentZoneName();
    var counterRun = 0;
    $('#poetry').css({'background': 'rgba(255,255,255,0.9)'});


    colorMode(HSB);
    eververseVis.visOutOfRangeConfig();
    counterRun = width + 500;
    
    for (var i = 0; i < counterRun; i++) {
        eververseVis.visOutOfRange();
    }

    /**
        Range visualisations removed until
        generative algo's are developed for 
        all bands (need about 6)
    */
    // switch (band){
    //     case 'Fat Burn':        
    //         eververseVis.visFatBurnConfig();
    //         eververseVis.visFatBurn();

    //         break;
    //     case 'Cardio':        
    //         eververseVis.visCardioConfig();

    //         counterRun = (width + height)  * 2;
    //         for (var i = 0; i < counterRun; i++) {
    //             eververseVis.visCardio();  
    //         }

    //         // eververseVis.visCardio();  
    //         break;
    //     case 'Peak':        
    //         eververseVis.visPeakConfig();
    //         // eververseVis.visPeak();     
    //         // var fr = frameRate() ;
    //         // counterRun = fr * 10;
    //         // console.log(counterRun);

    //         // // counterRun = width + 500;
    //         counterRun = 50;
    //         for (var i = 0; i < counterRun; i++) {
    //             eververseVis.visPeak();     
    //         }
            
    //         break;
    //     default:

    //         colorMode(HSB);
    //         eververseVis.visOutOfRangeConfig();

    //         counterRun = width + 500;
            
    //         for (var i = 0; i < counterRun; i++) {
    //             eververseVis.visOutOfRange();
    //         }

    //         break;
    // }

}

function draw()
{
   // not being used as vis are now being created
   // once to be used as an image, rather than 
   // happening as an animation 
}



/**
 * Re-run each time the window is resized
 */
function windowResized () 
{
    resizeCanvas (windowWidth, windowHeight);     
    setup();
}


function doInterpolate(cOne, cTwo, beTwixt) {
  var ptTemp = [];
  for (var j=0; j<eververseConfig.degs;j++) {
    ptTemp[j]=coslerp(cOne[j], cTwo[j], beTwixt);
  }
  // console.log(ptTemp);
  return ptTemp;
}

function doSimpleOutline(myCircle) {
  // eververseConfig.masterX= eververseConfig.masterX + 3.0;
  eververseConfig.masterX= eververseConfig.masterX + 1.0;
  doDrawCircle(parseInt(eververseConfig.masterX), parseInt(eververseConfig.masterY), myCircle);
}

function coslerp(val1, val2, amt) {
  amt = map (cos(amt*PI), 1.0, -1.0, 0, 1.0);
  return lerp(val1, val2, amt);
}


function doSetupModels() {
  for (var i=0;i<eververseConfig.models;i++) {
    // var ptTemp = new float[degs];
    var ptTemp = [];
    var zz=random(3);
    eververseConfig.radii[i]=doNewCircle(zz, zz-1);
  }
}



function doDrawCircle(xPos, yPos, ptRad) {  
  push();
  translate(xPos, yPos);
  // rotate(radians(frameCount*0.95125));
   // rotate(radians(frameCount*2.5));
  // rotate(radians(frameCount ));
  // rotate(radians());
  
  var col = eververseVis.getColor(xPos);
  
  stroke(col);
    
  beginShape();  

  for (var j=0; j<eververseConfig.degs; j=j+1) { 
    var rj=radians(j);
    var xA= sin(rj) * ptRad[(j)];
    var yA= cos(rj) * ptRad[(j)];
    vertex(xA, yA);
  }
  fill(eververseConfig.background);
  endShape();
  pop();
}


function doNewCircle(nXstart, nYstart) {
 var theCircle =[];
  // for (var i= 0 + parseInt(frameCount/2.0); i<eververseConfig.degs +int(frameCount/2.0) ;i++) {
  for (var i= 0; i<eververseConfig.degs ;i++) {
    var nX = sin(radians(i))* eververseConfig.nRadius+nXstart;
    var nY = cos(radians(i))* eververseConfig.nRadius+nYstart;
    theCircle[i]=map(noise(nX, nY), 0, 1, 0, eververseConfig.outRadius);
  }
  return theCircle;
}


