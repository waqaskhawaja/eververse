/**
 * Interact with moment.js to set readable time-since
 * on line updates.
 *
 * There should be an update every 4 (or so) lines - not with
 * every line (looks too cluttered)
 */

var eververseTime = {

    // Update the relative times set next to the text
    // any time a new piece of text is added
    // Also, only want them displayed every 3 rows...
    updateTimeDisplays: function(){

        if( ! eververseConfig.is_performance ){
            var totalCount = $('.time-since').length;

            $('.time-since').each(function(i){
                var $this = $(this);
                var time = moment($this.data('timestamp'));
                if (time.isValid() && i % 3 === 0){
                    $this.html( time.fromNow() ).fadeIn();
                }else{
                    $this.html('');
                }
            });

        }

    }
};