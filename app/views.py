import datetime

from flask import render_template, Response, stream_with_context, jsonify, request
from app import app
from app.generator.model_generator import write_poetry
from app.models import HeartZone, HeartEntry, SleepZone
from app.client.fitbit_client import read_fitbit, renew_token, sync_fitbit_server, get_total_heart_zone_dates
from datetime import datetime
import time
import json
import os
import csv

performance_log_path = './app/resources/performance-mode-data-log/'


@app.route('/')
@app.route('/index')
def index():
    values = read_fitbit()
    if values is None:
        values = [{'test': 'Some test data'}]
    return render_template("index.html", fitbit_data=json.dumps(values))


@app.route('/sync')
def sync():
    sync_fitbit_server()
    values = read_fitbit()
    if values is None:
        values = [{'test': 'Some test data'}]
    return render_template("index.html", fitbit_data=json.dumps(values))


@app.route('/getEnabledDates')
def get_enabled_dates():
    return jsonify(get_total_heart_zone_dates())


@app.route('/getVerseByDateTime')
def get_verse_by_date_time():
    date_time = datetime.strptime(request.args.get('selectedDate'), '%Y-%m-%d %H:%M')

    return_dict = {}
    heart_zone_dict = {}
    heart_entry_dict = {}

    heart_entry = HeartEntry.query.filter(HeartEntry.entry_date_time == date_time).first()
    heart_entry_dict["date_time"] = heart_entry.entry_date_time
    heart_entry_dict["value"] = heart_entry.value

    heart_zone = HeartZone.query \
        .filter(HeartZone.min_val < heart_entry.value) \
        .filter(HeartZone.max_val > heart_entry.value).first()
    heart_zone_dict["zone_name"] = heart_zone.zone_name
    heart_zone_dict["number_of_words"] = heart_zone.number_of_words
    heart_zone_dict["min_val"] = heart_zone.min_val
    heart_zone_dict["max_val"] = heart_zone.max_val
    heart_zone_dict["frequency_in_seconds"] = heart_zone.frequency_in_seconds

    poem = write_poetry(2, heart_zone.number_of_words, heart_zone.corpus.file_path)

    return_dict["heart_zone"] = heart_zone_dict
    return_dict["heart_zone_entry"] = heart_entry_dict
    return_dict["poem"] = poem

    return jsonify(return_dict)


# "Performance mode" waiting screen
@app.route('/performance-waiting')
def performance_waiting():
    return render_template("performance-waiting.html")


# "Performance mode" - gives a different
# frontend layout, and uses a json file (from /static/data) 
# as the datasource instead of the fitbit data
@app.route('/performance')
def performance():
    return render_template("performance.html")


# "Exhibition mode" - for use with a large display
# (Thunderbolt-size). Will display some blurb, and logos
@app.route('/exhibition')
def exhibition():
    return render_template("exhibition.html")


# -------------------------
# Static routes
# -------------------------
@app.route('/technical')
def technical():
    return render_template("technical-details.html")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/events-publications')
def events_publications():
    return render_template("events-publications.html")


# -------------------------
# / static routes
# -------------------------
@app.route('/_get_verse')
def get_verse():
    zone_value = request.args.get('zoneValue', 0, type=int)
    log_poem = request.args.get('logPoem', False, type=bool)

    heart_zone = HeartZone.query \
        .filter(HeartZone.min_val <= zone_value) \
        .filter(HeartZone.max_val >= zone_value) \
        .first()


    if heart_zone is not None:
        poem = write_poetry(2, heart_zone.number_of_words, heart_zone.corpus.file_path)

        # If the performance flag is set, need to log
        # the data to a file (for later retrieval & printing)
        

        output = {
            'poem': poem, 
            'sleepTime': heart_zone.frequency_in_seconds,
            'heartRate': request.args.get('zoneValue', 0, type=int),
            'timestamp': time.strftime('%Y-%m-%d %H:%M:%S')         # this should be the heart-rate timestamp
        }
        
        if log_poem is True:
            outputCopy = output
            output_poem_to_log(outputCopy)

        return jsonify(output)
    else:
        # -------------------------
        # RETURN TEST DATA
        # -------------------------
        return jsonify({
            'poem': [
                ['Beheld','and','smelled','the','towels,','Still','he','tries','to','prove'],
                ['Or','you','could','hold','everything','black.','You','begin','to','move']
            ],    
            'sleepTime': 60,
            'heartRate': request.args.get('zoneValue', 0, type=int),
            'timestamp': time.strftime('%Y-%m-%d %H:%M:%S')
        })


@app.route('/poetry')
def get_poetry():
    def stream_verses():
        values = read_fitbit()
        for item in values:
            for k, v in item.items():
                if k == 'value':
                    heart_zone = HeartZone.query \
                        .filter(HeartZone.min_val < v) \
                        .filter(HeartZone.max_val > v) \
                        .first()
                    poem = write_poetry(2, heart_zone.number_of_words)
                    for line in poem:
                        yield str(' '.join(line))
                        yield '<br/>'
                    yield '<br/>'
                    time.sleep(heart_zone.frequency_in_seconds)
    return Response(stream_with_context(stream_verses()))


# Need a way to get all zone data into
# the frontend
@app.route('/get_zones')
def get_zones():
    heart_zones = HeartZone.query.all()
    data = []
    for zone in heart_zones:
        data.append( {
            'name': zone.zone_name,
            'min': zone.min_val,
            'max': zone.max_val,
            'frequency_in_seconds': zone.frequency_in_seconds,
            'number_of_words': zone.number_of_words
        })
    return jsonify(data)


def output_poem_to_log(output):
    
    poem_data = output        
    headers = ['poem', 'poem_text', 'sleepTime', 'heartRate', 'timestamp']            
    filename = time.strftime('%Y-%m-%d' + '.csv')
    poem_text = ''
    for line in poem_data['poem']:
        poem_text += " ".join(line) + ' '

    poem_data['poem_text'] = poem_text
    
    output_to_file([poem_data], headers, performance_log_path, filename)

    return output


def output_to_file(results, headers, path, filename):

    # Make output directories if necessary
    if not os.path.exists(os.path.dirname(path)):
        try: 
            os.makedirs(path)
        except OSError:
            if not os.path.isdir(path):
                raise

    # If the file doesn't exist, create it and add the csv headers
    if not os.path.isfile(path + filename):
        with open(path + filename, 'a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
        csvfile.close()

    with open(path + filename, 'a') as csvfile:    
        writer = csv.DictWriter(csvfile, fieldnames=headers)        
        
        for r in results:
            writer.writerow(r)


@app.route('/api')
def api():
    activity_date = request.args.get('date')
    activity_time = request.args.get('time')
    activity_type = request.args.get('type')
    activity_value = request.args.get('value')
    activity_sleep_cycle_count = request.args.get('sleep_cycle_count',0, type=int)

    if activity_type == 'heart':
        zone = HeartZone.query \
            .filter(HeartZone.min_val <= activity_value) \
            .filter(HeartZone.max_val >= activity_value) \
            .first()


        if zone is not None:
            poem = write_poetry(2, zone.number_of_words, zone.corpus.file_path)
        else:
            return jsonify({'error': 'Matching HR Zone not found'})

    else:
        # sleep
        zone = SleepZone.query \
            .filter(SleepZone.sleep_zone_name == activity_value.lower()) \
            .first()

        if zone is not None:
            poem = write_poetry(2, zone.number_of_words, zone.corpus.file_path)
        else:
            return jsonify({'error': 'Matching Sleep Zone not found'})
            
    output = {
            'poem': poem,             
            'activity': activity_type,
            'value': activity_value ,
            'timestamp': activity_date + ' ' + activity_time,
            'num_words': zone.number_of_words,
            'sleep_cycle_count': activity_sleep_cycle_count,
            'corpus': zone.corpus.file_path
        }
    return jsonify(output)