import os
basedir = os.path.abspath(os.path.dirname(__file__))

WTF_CSRF_ENABLED = True
SECRET_KEY = 'eververse-secret-key'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

BASIC_AUTH_USERNAME = 'eververse'
BASIC_AUTH_PASSWORD = 'eyJhbGciOiJIUzI1NiJ9'