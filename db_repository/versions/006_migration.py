from sqlalchemy import *
from migrate import *

from migrate.changeset import schema

pre_meta = MetaData()
post_meta = MetaData()


sleep_zone = Table('sleep_zone', post_meta,
                   Column('id', Integer, primary_key=True, nullable=False),
                   Column('sleep_zone_name', String(length=32)),
                   Column('heart_zone_id', Integer),
                   )


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['sleep_zone'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['sleep_zone'].drop()

