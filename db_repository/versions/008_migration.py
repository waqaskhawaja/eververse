from sqlalchemy import *
from migrate import *

from migrate.changeset import schema

pre_meta = MetaData()
post_meta = MetaData()


sleep_entry = Table('heart_entry', post_meta,
                    Column('id', Integer, primary_key=True),
                    Column('entry_date_time', DateTime),
                    Column('value', Integer)
                    )


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['heart_entry'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['heart_entry'].drop()

