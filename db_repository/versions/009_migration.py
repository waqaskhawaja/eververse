from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()


sleep_zone = Table('sleep_zone', post_meta,
                   Column('id', Integer, primary_key=True, nullable=False),
                   Column('sleep_zone_name', String(length=32)),
                   Column('number_of_words', Integer),
                   Column('frequency_in_seconds', Integer),
                   Column('heart_zone_id', Integer),
                   )


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['sleep_zone'].columns['number_of_words'].create()
    post_meta.tables['sleep_zone'].columns['frequency_in_seconds'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['sleep_zone'].columns['number_of_words'].drop()
    post_meta.tables['sleep_zone'].columns['frequency_in_seconds'].drop()

