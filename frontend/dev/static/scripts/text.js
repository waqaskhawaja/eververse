



var eververseText = {

    scrollToBottom: true,

    sampleResponse: {
        'poem':[
                ['Beheld','and','smelled','the','towels,','Still','he','tries','to','prove'],
                ['Or','you','could','hold','everything','black.','You','begin','to','move']
        ],   
        'sleepTime':60,
        'heartRate': 76,
        'timestamp': '2018-03-21 14:23:15'
    },

    currentData: {},

    source: null,

    template: null,

    init: function(){
        if(this.source === null){
            this.source = document.getElementById('poetry-section-template').innerHTML;
        }

        if(this.template === null){
            this.template = Handlebars.compile(this.source);
        }

        $("#poetry").animate({
            scrollTop: ($('#poetry')[0].scrollHeight - $('#poetry')[0].clientHeight) + 600
        }, 1000);  
        

    },


    writeLine: function(data)
    {
        if(typeof data === 'undefined'){
            this.currentData = this.sampleResponse;
        }else{
            this.currentData = data;
        }

        var $poetry = $('#poetry');

        if(this.scrollToBottom){
            // Debug scroll positioning
            // console.log( 
            //     ($('#poetry')[0].scrollHeight - $('#poetry')[0].clientHeight) + 600 ,
            //     $('#poetry')[0].scrollHeight, 
            //     $('#poetry')[0].clientHeight 
            // ); 

            $("#poetry").animate({
              scrollTop: ($('#poetry')[0].scrollHeight - $('#poetry')[0].clientHeight) + 800
            }, 2000);            
        }

        let timestamp = moment(this.currentData.timestamp).format('YYYY-MM-DD HH:mm:ss')

        var initialData = {
            timestamp: timestamp,
            time_string: (eververseConfig.is_performance ) ? '' : moment(timestamp).fromNow(),
            line_1: this.currentData.poem[0].join(' ')
        }
        
        var html = this.template(initialData);
        
        console.log(data);
        // need to look at sleeptimes under 5 seconds,
        // and reduce animation times => animations not
        // getting chance to run.
        let fade = 600;
        let delayIn = 1200;
        let delayOut = 1800;

        if( data.sleepTime <= 5){
            fade = fade / 2;
            delayIn = delayIn / 2;
            delayOut = delayOut / 2;
        }

        if( data.sleepTime <= 3){
            fade = fade / 3;
            delayIn = delayIn / 3;
            delayOut = delayOut / 3;
        }

        $('#heartbeat')
            .removeClass('invisible')
            .fadeIn(fade)
            .delay(delayIn)
            .fadeOut(fade, function(){
                $poetry.append(html);
            })
            .delay(delayOut)
            .promise()
                .done(function(){
                    // console.log('second line display...: ' + eververseText.currentData.poem[1].join(' '));
                    $('#poetry .row:last').find('span.line-2').html( eververseText.currentData.poem[1].join(' ')).addClass('animated fadeInRight'); 
                    // console.log($('#poetry .row:last').find('span.line-2'));
                });
              
        
    }

};