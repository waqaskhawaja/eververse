/**
 * Code related to production of the generative 
 * design elements of Eververse
 *
 * @author David Kelly
 * 15/1/2018
 */

var eververseConfig = {    
    
    log_poem_data: false,           // do we want to save the text in backend?

    isPerformance: false,           // is this for a public performance?
    
    loop_text: false,               // run as a loop? (once end of data file is reached, go back to start)

    // currentHeartRateValue: 80,        // outOfRange
    // currentSleepValue: 5,
    currentValue: {
        'heart': 80,
        'sleep': 5
    },

    activeBiometric: 'heart',        // 'heart'  |  'sleep'
    
    zones: {
        'heart': [
            {
                'max': 91, 
                'name': 'OutOfRange', 
                'min': 30
            }, 
            {
                'max': 128, 
                'name': 'Fat Burn', 
                'min': 91
            }, 
            {
                'max': 155, 
                'name': 'Cardio', 
                'min': 128
            }, 
            {
                'max': 220, 
                'name': 'Peak', 
                'min': 155
            }
        ],
        'sleep': [
            {
                'name': 'Awake',
                'min': 1,
                'max': 3
            },
            {
                'name': 'Light Sleep',
                'min': 4,
                'max': 6
            },
            {
                'name': 'Deep Sleep',
                'min': 7,
                'max': 9
            },
            {
                'name': 'REM Sleep',
                'min': 10,
                'max': 12
            }

        ]

    },

    update: function(newDataValue, biometric)
    {
        // set the active biometric [heart | sleep]
        if (!_.isUndefined(biometric)){
            this.activeBiometric = biometric;
        }

        // set a value for it.
        this.currentValue[this.activeBiometric] = newDataValue;

        // do the bg generation
        setup();
    }
};

/**
 * Object used to manage the visualisation-related
 * functionality of Eververse
 */
var eververseVis = {

    everversePalettes: {
        // gradients from: https://uigradients.com
        // order of these is important.
        // names in comments:
        heart: [            
            ['#bfe9ff', '#ff6e7f'],          // Noon to Dusk
            ['#DBE6F6', '#C5796D'],          // Jaipur
            ['#e96443', '#904e95'],          // GrapefruitSunset
            ['#F15F79', '#B24592'],          // Blush
            ['#FF512F', '#DD2476'],          // BloodyMary
            ['#CB356B', '#BD3F32'],          // Alive
            ['#dd1818', '#333333']           // Pure lust
        ],
        sleep: [
            ['#43C6AC', '#191654'],          // TheBlueLagoon
            // ['#bdc3c7', '#2c3e50'],          // 50ShadesofGrey
            ['#2980b9', '#2c3e50'],          // Nighthawk
            ['#3d72b4', '#525252'],          // Curiosityblue
            ['#3A6073', '#16222A']           // Mirage
        ]
    },

    /**
     * get the name of the current heart-rate
     * zone. Returns default if none known.
     * 
     * @return string | default 'OutOfRange'
     */
    getCurrentZoneName: function(){

        var activeZones = eververseConfig.zones[eververseConfig.activeBiometric];
        var current = eververseConfig.currentValue[eververseConfig.activeBiometric];

        var zone = _.find(activeZones, function(value){
            return (current >= +value.min) && (current <= value.max);
        });

        if(zone){
            return zone.name;
        }else{
            return activeZones[0].name;
        }
    },
        

    /**
     * get the name of the current heart-rate
     * zone. Returns default if none known.
     * 
     * @return string | default 'OutOfRange'
     */
    getCurrentZone: function(){

        var activeZones = eververseConfig.zones[eververseConfig.activeBiometric];
        var current = eververseConfig.currentValue[eververseConfig.activeBiometric];

        var zone = _.find(activeZones, function(value){
            return (current >= +value.min) && (current <= value.max);
        });


        if(zone){
            return zone;
        }
        return activeZones[0];
  
    },

    /**
     * Find the array index for the current zone - used
     * when mapping background colours to zones
     *
     * @TODO Update Sleep case with correct zones
     */
    getCurrentZoneIndex: function(currentZoneName){
        var activeZones = eververseConfig.zones[eververseConfig.activeBiometric];
        var current = eververseConfig.currentValue[eververseConfig.activeBiometric];

        var zoneIndex = _.findIndex(activeZones,{ name: currentZoneName });

        if(zoneIndex){
            return zoneIndex;
        }
        return 0;
    },

    // 
    // =============================================
    // 
    


    /**
     * Uses canvas to create a linear gradient based
     * on an array of colours.
     */
    eververseBg: function(ctx, width, height, zone, zoneType)
    {
         // // Gradient foreground
        const fill = ctx.createLinearGradient(0, 0, width, height);
        const colours = this.generateEververseBackgroundColours(zone);
        // account for multi-coloured gradients (not planned, but...)
        const fillStepCount = 1 / (colours.length - 1);

        for (var i = 0; i < colours.length; i++) {
            fill.addColorStop(fillStepCount * i, colours[i]);
        }

        // add last colour stop
        fill.addColorStop(1, colours[colours.length - 1]);
        
        return fill;

    },


    /*
     * Handles the logic of which biometric / zone to use
     * in bg image generation. 
     */
    generateEververseBackgroundColours: function (zone){

        var zoneType = eververseConfig.activeBiometric;
        var zoneIndex = this.getCurrentZoneIndex(zone.name );
        var configZone = eververseConfig.zones[eververseConfig.activeBiometric];

        // map the zone we're in to a colour pallette index, based on 
        // how many zones we have, and how many palettes we have
        var backgroundIndexFloat = map(zoneIndex, 0, configZone.length - 1, 0, this.everversePalettes[zoneType].length - 1 );
        
        // it needs to be an integer to map to the array index...
        var backgroundIndex = Math.round(backgroundIndexFloat);

        return this.everversePalettes[zoneType][backgroundIndex];
    }

};



function setup()
{
    createCanvas (windowWidth, windowHeight);
    var context = document.getElementById("defaultCanvas0").getContext("2d")
    var zone = eververseVis.getCurrentZone();

    // make sure the text container is readable.
    $('#poetry').css({'background': 'rgba(255,255,255,0.9)'});
    
    // don't want gradient under the sidebar
    var content = $('#content').position();

    context.fillStyle = eververseVis.eververseBg(context, width, height, zone, eververseConfig.activeBiometric);
    context.fillRect(content.left, 0, width, height);

}

function draw()
{
   // not being used as vis are now being created
   // once to be used as an image, rather than 
   // happening as an animation 
}



/**
 * Re-run each time the window is resized
 */
function windowResized () 
{
    resizeCanvas (windowWidth, windowHeight);     
    setup();
}
