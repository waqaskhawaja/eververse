// generated on 2017-12-15 using generator-webapp 3.0.1
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let dev = true;

gulp.task('styles', () => {
  return gulp.src('frontend/dev/static/styles/*.scss')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.if(dev, $.sourcemaps.write()))
    .pipe(gulp.dest('.tmp/static/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('frontend/dev/static/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(dev, $.sourcemaps.write('.')))
    .pipe(gulp.dest('.tmp/static/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('frontend/dev/static/scripts/**/*.js')
    .pipe(gulp.dest('app/static/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['styles', 'scripts'], () => {
  return gulp.src('frontend/dev/*.html')
    .pipe($.useref({searchPath: ['.tmp/static', 'frontend/dev/static', '.']}))
    .pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
    .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if(/\.html$/, $.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('app'));
});

gulp.task('images', () => {
  return gulp.src('frontend/dev/static/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('app/static/images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('frontend/dev/static/fonts/**/*'))
    .pipe($.if(dev, gulp.dest('.tmp/static/fonts'), gulp.dest('app/static/fonts')));
});

gulp.task('extras', () => {
  return gulp.src([
    'frontend/dev/*',    
    '!frontend/dev/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('app/static'));
});



gulp.task('data', ['extras'], () => {
  return gulp.src([
    'frontend/dev/static/data/*.json'
  ], {
    dot: true
  }).pipe(gulp.dest('app/static/data'))
});

gulp.task('template_move', ['html', 'extras'], () => {
  return gulp.src([
    'app/index.html',
    'app/performance.html',
    'app/performance-waiting.html',
    'app/exhibition.html'
  ], {
    dot: true
  }).pipe(gulp.dest('app/templates'))
});

gulp.task('clean_template', ['template_move'], () => {
  return del('app/*.html');  
});



gulp.task('clean', del.bind(null, ['.tmp', 'app/static']));

gulp.task('serve', () => {
  runSequence(['clean'], ['styles', 'scripts', 'fonts'], () => {
    browserSync.init({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app/static'],
        routes: {
          '/node_modules': 'node_modules'
        }
      }
    });

    gulp.watch([
      'frontend/dev/*.html',
      'frontend/dev/static/images/**/*',
      '.tmp/static/fonts/**/*'
    ]).on('change', reload);

    gulp.watch('frontend/dev/static/styles/**/*.scss', ['styles']);
    gulp.watch('frontend/dev/static/scripts/**/*.js', ['scripts']);
    gulp.watch('frontend/dev/static/fonts/**/*', ['fonts']);
    // gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', ['default'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['app/static']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/static/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
// gulp.task('wiredep', () => {
//   gulp.src('app/styles/*.scss')
//     .pipe($.filter(file => file.stat && file.stat.size))
//     .pipe(wiredep({
//       ignorePath: /^(\.\.\/)+/
//     }))
//     .pipe(gulp.dest('app/styles'));

//   gulp.src('app/*.html')
//     .pipe(wiredep({
//       exclude: ['bootstrap'],
//       ignorePath: /^(\.\.\/)*\.\./
//     }))
//     .pipe(gulp.dest('app'));
// });

gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras', 'template_move', 'data', 'clean_template'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  return new Promise(resolve => {
    dev = false;
    // runSequence(['clean', 'wiredep'], 'build', resolve);
    runSequence(['clean'], 'build', resolve);
  });
});
